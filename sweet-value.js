Let a = 1;
Let b = 2;
Let c = a;

console.log("nilai awal dari variabel a " + a);
console.log("nilai awal dari variabel b " + b);

// Proses tukar nilai
a = b;
b = c;

console.log("nilai awal dari variabel a " + a);
console.log("nilai awal dari variabel b " + b);